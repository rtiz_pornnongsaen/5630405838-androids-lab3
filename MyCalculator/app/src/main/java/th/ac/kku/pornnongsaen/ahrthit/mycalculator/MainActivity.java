package th.ac.kku.pornnongsaen.ahrthit.mycalculator;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener  {
    Button cal;
    RadioButton plus, minus, multiple, divide;
    float num1, num2;
    TextView tvResult;
    EditText input1, input2;
    float result = 0;
    int i = 0;
    RadioGroup rg;
    Switch toggle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        cal = (Button) findViewById(R.id.cal);
        plus = (RadioButton) findViewById(R.id.addButton);
        minus = (RadioButton) findViewById(R.id.subButton);
        multiple = (RadioButton) findViewById(R.id.multiButton);
        divide = (RadioButton) findViewById(R.id.divButton);
        input1 = (EditText) findViewById(R.id.input1);
        input2 = (EditText) findViewById(R.id.input2);
        tvResult = (TextView) findViewById(R.id.result);
        rg = (RadioGroup) findViewById(R.id.radioGroup);
        toggle = (Switch) findViewById(R.id.switch1);

        cal.setOnClickListener(this);
        plus.setOnClickListener(this);
        minus.setOnClickListener(this);
        multiple.setOnClickListener(this);
        divide.setOnClickListener(this);
        toggle.setOnClickListener(this);

        toggle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (i==0){
                toggle.setText("ON");
                    i=1;
                }
                else {
                    toggle.setText("OFF");
                    i=0;
                }
            }
        });

        /*cal.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                num1 = Float.parseFloat(input1.getText().toString());
                num2 = Float.parseFloat(input2.getText().toString());

                if (plus.isChecked()) {
                    result = num1 + num2;
                } else if (minus.isChecked()) {
                    result = num1 - num2;
                } else if (multiple.isChecked()) {
                    result = num1 * num2;
                } else if (divide.isChecked()) {
                    result = num1 / num2;
                }
                tvResult.setText("= "+ new Float(result).toString());
            }
        });
        plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                num1 = Float.parseFloat(input1.getText().toString());
                num2 = Float.parseFloat(input2.getText().toString());
                result = num1 + num2;
                tvResult.setText("= "+ new Float(result).toString());
            }
        });
        minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                num1 = Float.parseFloat(input1.getText().toString());
                num2 = Float.parseFloat(input2.getText().toString());
                result = num1 - num2;
                tvResult.setText("= "+ new Float(result).toString());
            }
        });
        multiple.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                num1 = Float.parseFloat(input1.getText().toString());
                num2 = Float.parseFloat(input2.getText().toString());
                result = num1 * num2;
                tvResult.setText("= "+ new Float(result).toString());
            }
        });
        divide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                num1 = Float.parseFloat(input1.getText().toString());
                num2 = Float.parseFloat(input2.getText().toString());
                result = num1 / num2;
                tvResult.setText("= "+ new Float(result).toString());
            }
        });*/



    }

    private void showToast(String msg){
        Toast.makeText(MainActivity.this,msg,Toast.LENGTH_LONG).show();
    }

    @Override
    public void onClick(View v) {
        try{
        num1 = Float.parseFloat(input1.getText().toString());
        num2 = Float.parseFloat(input2.getText().toString());
        int i = 0;

        switch (rg.getCheckedRadioButtonId()){
            case R.id.addButton:
                result = num1+num2;
                break;
            case R.id.subButton:
                result = num1-num2;
                break;
            case R.id.multiButton:
                result = num1*num2;
                break;
            case R.id.divButton:
                if (num2!=0){
                result = num1/num2;
                break;}
                else showToast("Please divide by a non-zero number");
        }
        tvResult.setText("= "+ new Float(result).toString());
            showToast("Result ="+result);
        }catch (NumberFormatException e){
            showToast("Please enter only a number");
        }

    }
}